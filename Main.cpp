#include <iostream>
#include "Transport.h"
#include "Horse.h"

int main()
{
    //Create Transport pointer
    Transport* t;

    //Create new horse object in heap and get pointer to it
    Horse* h = new Horse();

    //Use Polymorphism by storing the child class in parent class pointer
    t = h;

    //TEST CASE 1 - Calling go on horse object pointer 
    //Expected: start at 0 and move 100km
    std::cout << "Distance travelled before go() is called on horse object pointer: " << h->get_dist_travelled() << std::endl;
    h->go();
    std::cout << "Distance travelled after go() is called on horse object pointer: " << h->get_dist_travelled() << std::endl;

    //TEST CASE 2 - Calling go on parent object pointer that stores the horse object pointer
    //Expected: start at 100 (from last test) and move to 200km
    std::cout << "Distance travelled before go() is called on horse object pointer stored in parent pointer: " << t->get_dist_travelled() << std::endl;
    t->go();
    std::cout << "Distance travelled after go() is called on horse object pointer stored in parent pointer: " << t->get_dist_travelled() << std::endl;

    /**
     *  The reason why coding example is important is that it highlights the fact that
     *  if I create another transport child class ie car I can have a differnt go()
     *  function within car, but still treat all of the children
     *  as Transport objects. This lets me place them all within the same array/vector or
     *  to treat them as Transport when passing them into a function that should accept
     *  any type of transport
     */
    
}
#include "Transport.h"

Transport::Transport()
{
    //Make sure to init private variables
    distance_travelled = 0;
}

int Transport::get_dist_travelled()
{
    return distance_travelled;
}

void Transport::go()
{
    //Unimplimented expect child class to impliment
}
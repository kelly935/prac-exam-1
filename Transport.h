#ifndef _TRANSPORT_H_
#define _TRANSPORT_H_

class Transport
{
    public:
        Transport();
        
        //Gets the private variable distance travelled and returns it
        int get_dist_travelled();
        //Virtual function to be overriden by child class Horse
        virtual void go();

    /**
     * Make sure to use protected for variables we want only child classes to see
     * We do not put distance travelled in private: as then Horse 
     * child class would not be able to access it
    */
    protected:
        int distance_travelled;
};

#endif
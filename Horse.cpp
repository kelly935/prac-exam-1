#include "Horse.h"

//Makes sure to call parent constructor so inherited variables are initalised
Horse::Horse():Transport()
{

}

//I will assume that by "travelling 100km this means each time go is called"
void Horse::go()
{
    distance_travelled += 100;
}
#ifndef _HORSE_H_
#define _HORSE_H_

#include "Transport.h"

class Horse : public Transport
{
    public:
        Horse();
        
        //Horse overrides this function with its own implimentation
        void go() override;

    private:

};

#endif